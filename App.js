const express = require('express');
const bodyParser = require('body-parser')
const users = require('./models/users');
const Validator = require('jsonschema').Validator;

const userSchema = require("./validation/userSchema");

const v = new Validator();

const app = express();
app.use(express.json());
app.use(bodyParser.json());

app.post('/api/validation', (req,res) => {
	const validation = v.validate(req.body, userSchema, {throwError: true});
	res.send(validation);
})

app.listen(3000, () => {
  	console.log('Example app listening on port 3000!');
});